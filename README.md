# aklive

#### 介绍
自用直播源

#### 直播源地址

1. [jx_cn.txt](https://gitlab.com/xiaoluoxxx/aklive/-/raw/master/jx_cn.txt "https://gitlab.com/xiaoluoxxx/aklive/-/raw/master/jx_cn.txt")
2. [jx_cn.m3u](https://gitlab.com/xiaoluoxxx/aklive/-/raw/master/jx_cn.m3u "https://gitlab.com/xiaoluoxxx/aklive/-/raw/master/jx_cn.m3u")
3. [zb.txt](https://gitlab.com/xiaoluoxxx/aklive/-/raw/master/zb.txt "https://gitlab.com/xiaoluoxxx/aklive/-/raw/master/zb.txt")


#### 使用说明

1. 在DIYP软件内输入网址！
2. xiaoluoxxx


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
